# Base Module StyleGuide dev helper

## Principe

L'application va copier le contenu nécessaire depuis le dossier du projet `BaseModule`, vers le dossier du projet actuel, de façon à avoir un environnement d'intégration css plus réactif, tout en gardant la synchronisations des fichiers modifiés avec le projet source :  

- compilation des fichiers .scss  
- live-reload de la page template pour visualiser immédiatement le résultat  
- synchronisation permanente des fichiers édités vers le projet source  
    (par défault, la synchronisation s'effectue sur le fichier styleguide.tpl, et les fichiers .scss et css)

## Installation

### 1) Installer Node.js

[Node.js](https://nodejs.org/en/)  

Tester sur une version `12.x.x`, peut être soucis sur des versions inféreures à `10.x.x`, ou pas ?!
 
### 2) Cloner le projet
Idéalement, cloner le projet au même niveau que le dossier du projet `BaseModule` (facultatif)  
```text
dossier-repos-git/
|-BaseModule
|-infopro.styleguide-dev-helper
```  

```bash
git clone https://gitlab.com/public-land/infopro.styleguide-dev-helper.git
```

### 3) Créer un nouveau projet PHPStorm *infopro.styleguide-dev-helper*

### 4) Installer les dépendances

Lancer un terminal depuis PHPStorm (le terminal se placera automatiquement à la racine de notre projet), et lancer la commande:

```bash
npm install
```  

### 5) Setup de l'environnement

Il faut éditer le `package.json` et le script principal `setup-and-watch.js` en fonction des fichiers sur lesquels on travaille.

#### Compilation SASS ( `package.json` &  `setup-and-watch.js` )

Editer le fichier `package.json` et modifier les chemins du fichier .sass principal et son  chemin de sortie compilé en .css :

- ligne 7 , script "sass-design-sys" 


Editer le script `setup-and-watch.js` et modifier  

- ligne 10 , nom du dossier sass de travail 
- ligne 11 , nom du fichier `.scss` principal 


#### Chemin du dossier cible + Chemin du fichier .tpl (`setup-and-watch.js`) 

Editer le script `setup-and-watch.js` et modifier  
 
- le chemin relatif vers le dossier du projet cible  `BaseModule` , ligne 8   
- le chemin relatif vers le fichier .tpl du projet cible `BaseModule` , ligne 9   

### 6) Désactiver le "watcher" sass de PHPStorm

Dans la mesure ou le script de dev compile les fichiers `.scss` et copie le resultat dans le projet `BaseModule`, il est inutile d'activer le "watcher" de l'IDE.    

Biensur, à re-activer si on decide de travailler uniquement depuis le projet `BaseModule`.  

## Usage

### *** ! AVERTISSEMENTS ! ***  

1) Au lancement du script, les premières instructions, copies des fichiers du dossier source (`BaseModule`) vers notre dossier de travail de destination (`infopro.styleguide-dev-helper`).  
    Ainsi, pour toutes les opérations suivantes :  
     - Créations de nouveaux dossier/fichiers
     - suppression de dossier/fichiers
     - Renommage de dossier/fichiers  
     
    Il faudra penser à arreter le script, faire les opérations cités ci-dessus depuis le projet cible `BaseModule`, et une fois fait, relancer le script de dev.  
    
    De manière générale, **ne pas faire tourner le script** quand on travaille sur le projet cible `BaseModule`.  
    
2) Le script détecte, toutes les modifications apportées à des fichiers pour les copier vers notre dossier cible `BaseModule`.
    Ainsi, **si le scripts de tourne pas**, et que l'on fait des mofifs depuis notre projet de dev `infopro.styleguide-dev-helper`, au prochain lancement du script, **on écrasera toutes nos modifs** par les fichiers du projet cible inchangés `BaseModule`.
    
    De manière générale, **bien vérifier que le script tourne** quand on travaille depuis le projet de dev `infopro.styleguide-dev-helper`.  
    
### Lancer le script

Depuis la fenêtre d'un terminal, à la racine du projet de dev `infopro.styleguide-dev-helper` :  

```bash
npm run serve    
```  

- RDV au localhost:3031 ou 127.0.0.1:3031 dans un browser, et travailler avec le live-reload!
 
Arreter le script  
`ctrl+c`    
 