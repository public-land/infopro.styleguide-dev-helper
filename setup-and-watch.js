const copydir = require('copy-dir');
const watch = require('node-watch');
const fs = require('fs');

//***************************************
//  1 ) A ÉDITER - ci dessous
//***************************************
const dossierCible = "../BaseModule";
const tplFileCible = "../BaseModule/src/BaseModule/View/BaseModule/Static/styleguide-article.tpl";
const sassFolder = "edito-page";
const sassMainFileName = "edito-page.scss";
//***************************************
//  2 ) A ÉDITER
//  Modifer le chemin des fichier : .scss cible + .css cible
//  dans le fichier fichier package.json / script : "sass-design-sys"
//***************************************
//  3) Ouvrir un terminal à la racine de ce projet
//      et lancer le script avec la commande : $ npm run serve
//***************************************
//  4) RDV localhost:3031
//***************************************

//==============
// creation des dossiers + copie es fichiers :  .css , .images, .fonts, .js et .scss
fs.mkdirSync("./css/base-module",{ recursive: true });
copydir.sync(`${dossierCible}/public/css`, './css/base-module')

fs.mkdirSync("./images/base-module",{ recursive: true });
copydir.sync(`${dossierCible}/public/images`, './images/base-module')

fs.mkdirSync("./fonts/base-module",{ recursive: true });
copydir.sync(`${dossierCible}/public/fonts`, './fonts/base-module')

fs.mkdirSync("./js/base-module",{ recursive: true });
copydir.sync(`${dossierCible}/public/js`, './js/base-module')


fs.mkdirSync("./sass",{ recursive: true });
copydir.sync(`${dossierCible}/public/sass/${sassFolder}`, `./sass/${sassFolder}`)
fs.copyFileSync(`${dossierCible}/public/sass/${sassMainFileName}`, `./sass/${sassMainFileName}`);


//==============  copy & rename [styleguide].tpl to index.html
fs.copyFileSync(tplFileCible, './index.html');



//--- WATCH and copy
watch('./', {
    recursive: true,
    filter: f => !/node_modules/.test(f)
}, function (evt, modifiedFile) {

    //--- copy index.html to ... styleguide.tpl
    if(modifiedFile === "index.html") {
        fs.copyFileSync('./index.html', tplFileCible);
    }

    //--- copy sass
    else if(modifiedFile.indexOf(".scss") != -1 ){
        let filePath = modifiedFile.replace(/\\/g,"/");
        fs.copyFileSync(filePath, `${dossierCible}/public/${filePath}`);
    }

    //--- copy css generated
    else if(modifiedFile.indexOf(".css") != -1 ){
        let filePath = modifiedFile.replace(/\\/g,"/");
        let fileName = modifiedFile.split("\\")[modifiedFile.split("\\").length-1];
        fs.copyFileSync(filePath, `${dossierCible}/public/css/${fileName}`);
    }

});
